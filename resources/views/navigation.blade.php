<router-link tag="h3"
             :to="{name: 'fwb-school-organisation'}"
             class="cursor-pointer flex items-center font-normal dim text-white text-base no-underline"
>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 20 20" class="sidebar-icon">
        <defs>
            <path id="b" d="M11 18v-5H9v5c0 1.1045695-.8954305 2-2 2H4c-1.1045695 0-2-.8954305-2-2v-7.5857864l-.29289322.2928932c-.39052429.3905243-1.02368927.3905243-1.41421356 0-.3905243-.3905243-.3905243-1.02368929 0-1.41421358l9-9C9.48815536.09763107 9.74407768 0 10 0c.2559223 0 .5118446.09763107.7071068.29289322l9 9c.3905243.39052429.3905243 1.02368928 0 1.41421358-.3905243.3905243-1.0236893.3905243-1.4142136 0L18 10.4142136V18c0 1.1045695-.8954305 2-2 2h-3c-1.1045695 0-2-.8954305-2-2zm5 0V8.41421356l-6-6-6 6V18h3v-5c0-1.1045695.8954305-2 2-2h2c1.1045695 0 2 .8954305 2 2v5h3z"></path>
            <filter id="a" width="135%" height="135%" x="-17.5%" y="-12.5%" filterUnits="objectBoundingBox">
                <feOffset dy="1" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="1"></feGaussianBlur>
                <feColorMatrix in="shadowBlurOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.166610054 0"></feColorMatrix>
            </filter>
        </defs>
        <g fill="none" fill-rule="evenodd">
            <use fill="#000" filter="url(#a)" xlink:href="#b"></use>
            <use fill="var(--sidebar-icon)" xlink:href="#b"></use>
        </g>
    </svg>
    <span class="sidebar-label">
        @lang('nova Menü.Übersicht')
    </span>
</router-link>

<ul class="list-reste mb-8">
    <li class="leading-tight mb-4 ml-8 text-sm">
        <router-link tag="a"
                     :to="{name: 'fwb-school-organisation.classes'}"
                     class="text-white text-justify no-underline dim"
        >
            <span class="pr-12" id="class-overview-label">@lang('nova Menü.Klassen')</span>
        </router-link>
    </li>
    <li class="leading-tight mb-4 ml-8 text-sm">
        <router-link tag="a"
                     :to="{name: 'fwb-school-organisation.students'}"
                     class="text-white text-justify no-underline dim"
        >
            @lang('nova Menu.Teilnehmer')
        </router-link>
    </li>
</ul>
<!--  -->
<h3 class="flex items-center font-normal text-white mb-6 text-base no-underline">
	<svg class="sidebar-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
    	<path fill="var(--sidebar-icon)" d="M19.48 13.03A4 4 0 0 1 16 19h-4a4 4 0 1 1 0-8h1a1 1 0 0 0 0-2h-1a6 6 0 1 0 0 12h4a6 6 0 0 0 5.21-8.98L21.2 12a1 1 0 1 0-1.72 1.03zM4.52 10.97A4 4 0 0 1 8 5h4a4 4 0 1 1 0 8h-1a1 1 0 0 0 0 2h1a6 6 0 1 0 0-12H8a6 6 0 0 0-5.21 8.98l.01.02a1 1 0 1 0 1.72-1.03z"/>
	</svg>
    <span class="sidebar-label">{{ __('nova Menü.hilfreiche Links') }}</span>
</h3>