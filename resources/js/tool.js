import VueShepherd from 'vue-shepherd';
import VueCookie from 'vue-cookie';

Nova.booting((Vue, router, store) => {
    Vue.use(VueShepherd);
    Vue.use(VueCookie);

    router.addRoutes([
        {
            name: 'fwb-school-organisation',
            path: '/fwb-school-organisation',
            component: require('./components/Tool'),
        },
        {
            name: 'fwb-school-organisation.classes',
            path: '/fwb-school-organisation/classes',
            component: require('./components/Classes/Classes'),
        },
        {
            name: 'fwb-school-organisation.class',
            path: '/fwb-school-organisation/class/:id',
            component: require('./components/Classes/Class'),
        },
        {
            name: 'fwb-school-organisation.class.create',
            path: '/fwb-school-organisation/class/create',
            component: require('./components/Classes/CreateClass'),
        },
        {
            name: 'fwb-school-organisation.class.edit',
            path: '/fwb-school-organisation/class/:id/edit',
            component: require('./components/Classes/EditClass'),
        },
        {
            name: 'fwb-school-organisation.students',
            path: '/fwb-school-organisation/students',
            component: require('./components/Students/Students'),
        },
        {
            name: 'fwb-school-organisation.student.create',
            path: '/fwb-school-organisation/class/:id/student/create',
            component: require('./components/Students/CreateStudent'),
        }
    ]);
})
