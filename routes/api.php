<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use External\FwbSchoolOrganisation\Http\Controllers\FwbSchoolOrganisationController;
use External\FwbSchoolOrganisation\Http\Controllers\FwbSchoolOrganisationStatisticController;
use External\FwbSchoolOrganisation\Http\Controllers\FwbSchoolOrganisationStudentController;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Class related routes
Route::get('/school', FwbSchoolOrganisationController::class.'@getSchool')->name('school');
Route::get('/classes', FwbSchoolOrganisationController::class.'@getClasses')->name('classes.all');
Route::get('/classes-dropdown', FwbSchoolOrganisationController::class.'@getClassesForDropdown')->name('classes.all-dropdown');
Route::get('/class/{id}', FwbSchoolOrganisationController::class.'@getClass')->name('classes.single-class');
Route::put('/class/{id}', FwbSchoolOrganisationController::class.'@updateClass')->name('classes.update-class');
Route::post('/class', FwbSchoolOrganisationController::class.'@createClass')->name('classes.create-class');
Route::delete('/class/{id}', FwbSchoolOrganisationController::class.'@deleteClass')->name('classes.delete-class');
Route::get('/class/{id}/signup-status', FwbSchoolOrganisationController::class.'@getClassSingupStatus')->name('classes.class-signup-status');
Route::post('/class/{id}/signup/{challengeId}', FwbSchoolOrganisationController::class.'@signupClassToChallenge')->name('classes.class-singup-to-challenge');

// Student related routes
Route::get('/students', FwbSchoolOrganisationStudentController::class.'@getAllStudentsForAdmin')->name('students.school-all');
Route::get('/{classId}/students/{withCurrentChallengeRegistration?}', FwbSchoolOrganisationStudentController::class.'@getClassStudents')->name('students.class-all');
Route::post('/{classId}/students/excel-import', FwbSchoolOrganisationStudentController::class.'@importClassStudentsFromExcel')->name('students.class-excel-import');
Route::post('/student', FwbSchoolOrganisationStudentController::class.'@createStudent')->name('students.create-student');
Route::get('/{classId}/student/{id}/signup', FwbSchoolOrganisationStudentController::class.'@signUpStudentForChallenge')->name('students.student-signup-to-challenge');
Route::get('/{classId}/student/{id}/signoff', FwbSchoolOrganisationStudentController::class.'@signOffStudentFromChallenge')->name('students.student-signoff-from-challenge');
Route::post('/available-students/{classId}', FwbSchoolOrganisationStudentController::class.'@getAvailableStudents')->name('students.available-students');
Route::delete('/student/{studentId}', FwbSchoolOrganisationStudentController::class.'@deleteStudent')->name('students.delete-student');

// Excel download
Route::get('/excel-template', FwbSchoolOrganisationController::class.'@downloadExcelTemplate')->name('class.excel-template-download');

// Statistics for classes - for chart
Route::get('/classes-statistics', FwbSchoolOrganisationStatisticController::class.'@getChartStatistics')->name('classes-statistics');

// Get logged in User id
Route::get('/user', FwbSchoolOrganisationController::class.'@getLoggedInUser')->name('school.admin');
