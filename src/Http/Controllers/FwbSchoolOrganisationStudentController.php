<?php

namespace External\FwbSchoolOrganisation\Http\Controllers;

use App\Bike;
use App\Challenge;
use App\Helpers\CollectionHelper;
use App\Organisation;
use App\Person;
use App\Registration;
use App\Ride;
use Carbon\Carbon;
use External\FwbSchoolOrganisation\FwbSchoolOrganisationService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Nova\Http\Requests\NovaRequest;

class FwbSchoolOrganisationStudentController
{
    /**
     * @var FwbSchoolOrganisationService
     */
    public FwbSchoolOrganisationService $fwbService;

    /**
     * FwbSchoolOrganisationStudentController constructor.
     *
     * @param FwbSchoolOrganisationService $fwbService
     */
    public function __construct(FwbSchoolOrganisationService $fwbService)
    {
        $this->fwbService = $fwbService;
    }

    /**
     * Create Student and attach the Organisation
     *
     * @param NovaRequest $request
     */
    public function createStudent(NovaRequest $request)
    {
        // Validate inputs, return errors on failed validation
        $validation = $this->validateStudent($request->all());
        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        /** @var Organisation $class */
        $class = Organisation::query()->find($request->input('organisation_id'));

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        /** @var Person $student */
        $student = Person::query()->create([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'street' => $request->input('street'),
            'housenr' => $request->input('housenr'),
            'postcode' => $request->input('postcode'),
            'city' => $request->input('city'),
            'phonenr' => $request->input('phonenr'),
        ]);

        if (!is_null($class) && !is_null($schoolChallenge)) {
            Registration::query()->create([
                'challenge_id' => $schoolChallenge->id,
                'person_id' => $student->id,
                'organisation_id' => $class->id
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Validate student on creation
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateStudent(array $data)
    {
        $rules = [
            // Firstname and lastname
            'firstname' => ['required', 'string', 'max:100'],
            'lastname' => ['required', 'string', 'max:100'],
            // Password
            'password' => ['required', 'string', 'min:5'],
            // Address fields
            'phonenr' => ['max:150'],
            'street' => ['required','string','max:100', 'min:2'],
            'housenr' => ['required','string','max:20'],
            'postcode' => ['required', 'digits_between:4,5'],
            'city' => ['required','string','max:100', 'min:2'],
        ];
        $messages = [
            // Firstname and lastname
            'firstname.required' => trans('Onboarding.Pflichtfeld'),
            'lastname.required' => trans('Onboarding.Pflichtfeld'),
            // Password
            'password.required' => trans('Onboarding.Pflichtfeld'),
            'password.min' => trans('Logindaten ändern.Bitte geben Sie ein Passwort an, das mindestens 5 Zeichen hat.'),
            // Address fields
            'street.required' => trans('Onboarding.Pflichtfeld'),
            'housenr.required' => trans('Onboarding.Pflichtfeld'),
            'postcode.required' => trans('Onboarding.Pflichtfeld'),
            'postcode.digits_between' => trans('Onboarding.PLZ Länge'),
            'city.required' => trans('Onboarding.Pflichtfeld')
        ];

        // Email and/or username rules and messages
        if (isset($data['email'])) {
            if (Person::query()->where('email', '=', $data['email'])->exists()) {
                $rules = array_merge($rules, [
                    'email' => ['required', 'email', 'max:100'],
                    'username' => ['required', 'string', 'min:3', 'max:255', 'unique:people'],
                ]);
                $messages = array_merge($messages, [
                    'username.required' => trans('Logindaten ändern.Benutzername erforderlich'),
                    'username.min' => trans('Logindaten ändern.Der Benutzername muss aus mindestens 3 Zeichen bestehen.'),
                    'username.unique' => trans('Logindaten ändern.Dieser Benutzername existiert bereits - bitte geben Sie einen anderen an.')
                ]);
            } else {
                $rules = array_merge($rules, [
                    'email' => ['required', 'email', 'max:100', 'unique:people'],
                ]);
                $messages = array_merge($messages, [
                    'email.unique' => trans('Onboarding.TexterklaerungBenutzernameAngeben')
                ]);
            }
        } else {
            $rules = array_merge($rules, [
                'username' => ['required', 'string', 'min:3', 'max:255', 'unique:people']
            ]);
            $messages = array_merge($messages, [
                'username.required' => trans('Logindaten ändern.Benutzername erforderlich'),
                'username.min' => trans('Logindaten ändern.Der Benutzername muss aus mindestens 3 Zeichen bestehen.'),
                'username.unique' => trans('Logindaten ändern.Dieser Benutzername existiert bereits - bitte geben Sie einen anderen an.')
            ]);
        }

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Validate excel file
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateExcelFile(array $data)
    {
        $rules = [
            'people_list' => ['required', 'mimes:csv,xlsx,xls,ods'],
        ];

        $messages = [
            'people_list.required' => trans('Onboarding.Pflichtfeld'),
            'people_list.mimes' => 'Non suported type, please add .csv, .xlsx, .xls or .ods file'
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Import students in class from excel
     *
     * @param $classId
     * @param NovaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importClassStudentsFromExcel($classId, NovaRequest $request)
    {
        // Validate excel file, return errors on failed validation
        $validation = $this->validateExcelFile($request->all());
        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        /** @var Organisation $class */
        $class = Organisation::query()->find($classId);

        // Get school Challenge for Stakeholder
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        // Process excel
        $people = $this->fwbService->createPeopleFromExcel($request->file('people_list'));

        // return excel validation error if there is some
        if(!($people instanceof Collection) && (is_array($people) && array_key_exists('danger', $people))) {
            return response()->json([
                'errors' => ['people_list' => [$people['danger']]]
            ]);
        }

        // Create people registrations to the organisation and challenge (aka class)
        foreach ($people as $person) {
            $registration = new Registration();
            $registration->person_id = $person->id;
            $registration->organisation_id = $classId;
            if (!is_null($schoolChallenge)) {
                $registration->challenge_id = $schoolChallenge->id;
            }
            $registration->save();
            unset($registration);
        }

        return response()->json([
            'succes' => true
        ]);
    }

    /**
     * Get all class students
     *
     * @param $classId
     * @param null $withCurrentChallengeRegistration
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClassStudents($classId, $withCurrentChallengeRegistration = null)
    {
        /** @var Organisation $class */
        $class = Organisation::query()->find($classId);

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        // Get all students for this class with registration for the current or predecessor (if any) challenge
        $students = $this->fwbService->getClassStudents($classId, $schoolChallenge, $withCurrentChallengeRegistration);

        // Reject ones with registration to the challenge with another class
        $students = $students->reject(function (Person $student) use ($classId, $schoolChallenge) {
            return $student->registrations()
                ->where('organisation_id', '!=', $classId)
                ->where('challenge_id', '=', $schoolChallenge->id)->exists();
        });

        // Attach challenge statistics, latest ride and registration status
        /** @var Person $student */
        foreach ($students as $student) {
            // Attach statistics for the challenge
            $student->challenge_statistics = $student->getStatisticsForChallenge($schoolChallenge);

            // Attach last ride for the challenge
            /** @var Bike $bike */
            $primaryBike = $student->getPrimaryBike();
            $latestRide = Ride::query()
                ->where([
                    ['person_id', '=', $student->id],
                    ['bike_id', '=', $primaryBike->id],
                    ['created_at', '>', $schoolChallenge->start],
                    ['created_at', '<', $schoolChallenge->end]
                ])
                ->latest()
                ->first();

            if (!is_null($latestRide)) {
                $student->latest_ride = Carbon::parse($latestRide->datetime)->format('d.m.Y');
            }

            // Attach signup status
            $registration = Registration::query()
                ->where([
                    ['organisation_id', '=', $classId],
                    ['person_id', '=', $student->id],
                    ['challenge_id', '=', $schoolChallenge->id]
                ])
                ->first();

            if (!is_null($registration)) {
                $student->registered = true;
            } else {
                $student->registered = false;
            }

            // Attach impersonation
            if ($student->canBeImpersonatedNova()) {
                $student->impersonation = true;
            } else {
                $student->impersonation = false;
            }

            // Attach impresonate url
            $student->impersonate_url = $student->getImpersonateRedirect();
        }

        return response()->json($students);
    }

    /**
     * Get all students from all school classes for Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllStudentsForAdmin()
    {
        /** @var Person $admin */
        $admin = Auth::user();

        $classes = $this->fwbService->getClassesForAdmin();

        $students = new Collection();

        if ($classes->isNotEmpty()) {
            $parent = null;
            // Try to get parent from administered classes
            /** @var Organisation $class */
            foreach ($classes as $class) {
                if ($class->hasParent()) {
                    $parent = $class->parent;
                    break;
                }
            }
            // If there is a parent, take id in query
            if (!is_null($parent)) {
                $parentId = $parent->id;
            } else { // Otherwise, parent is first class from the admin's $classes collection
                $parent = $classes->first();
                $parentId = $parent->id;
            }

            // Take all child organisations (classes) of
            // parent and also add parent to collection
            $classes = Organisation::query()->where('parent_id', '=', $parent->id)->get();
            $classes->add($parent);

            // Get school Challenge
            $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($classes->first());

            if ($schoolChallenge->hasPredecessor()) {
                $challengeIds = [$schoolChallenge->id, $schoolChallenge->predecessor->id];
            } else {
                $challengeIds = [$schoolChallenge->id];
            }

            $students = new Collection();

            // Add all students from all classes into one collection
            /** @var Organisation $class */
            foreach ($classes as $class) {
                // Get all students for this class with registration for the current or predecessor (if any) challenge
                $classStudents = $this->fwbService->getClassStudents($class->id, $schoolChallenge);
                $students = $students->merge($classStudents);
            }

            // Add students which have been created by this user
            // (especially for the case if they have no class registration)
            $createdStudents = Person::query()
                ->whereHas('audits', function($query) use ($admin) {
                    $query->where('person_id', $admin->id)
                        ->where('event', 'created');
                })->get();

            $students = $students->merge($createdStudents);

            // Attach challenge statistics, latest ride and class
            /** @var Person $student */
            foreach ($students as $student) {
                // Attach statistics for the challenge
                $student->challenge_statistics = $student->getStatisticsForChallenge($schoolChallenge);

                // Attach last ride for the challenge
                /** @var Bike $bike */
                $primaryBike = $student->getPrimaryBike();
                $latestRide = Ride::query()
                    ->where([
                        ['person_id', '=', $student->id],
                        ['bike_id', '=', $primaryBike->id],
                        ['created_at', '>', $schoolChallenge->start],
                        ['created_at', '<', $schoolChallenge->end]
                    ])
                    ->latest()
                    ->first();

                if (!is_null($latestRide)) {
                    $student->latest_ride = Carbon::parse($latestRide->datetime)->format('d.m.Y');
                }

                // Attach class from latest registration
                $registration = Registration::query()
                    ->where('person_id', '=', $student->id)
                    ->whereIn('challenge_id', $challengeIds)
                    ->latest()
                    ->first();

                // attach challenge related data if student is or was registered to a challenge
                if ($registration) {
                    // Fetch only name and created_at - that's all we need
                    $studentClass = Organisation::query()
                        ->where('id', '=', $registration->organisation_id)
                        ->first(['name', 'created_at']);

                    $student->class_id = $registration->organisation_id;
                    if (!is_null($studentClass)) {
                        if ($studentClass->name) {
                            $student->class_name = $studentClass->name;
                        }
                        // Attach class creation year
                        if ($studentClass->created_at) {
                            $student->class_created_year = Carbon::parse($studentClass->created_at)->year;
                        }
                    }

                    // Attach school year if any
                    if ($registration->challenge_id == $schoolChallenge->id) {
                        $student->last_participated_challenge_year = Carbon::parse($schoolChallenge->end)->year;
                    } elseif ($registration->challenge_id == $schoolChallenge->predecessor->id) {
                        $student->last_participated_challenge_year = Carbon::parse($schoolChallenge->predecessor->end)->year;
                    }
                }

                // Attach impersonation
                if ($student->canBeImpersonatedNova()) {
                    $student->impersonation = true;
                } else {
                    $student->impersonation = false;
                }

                // Attach impresonate url
                $student->impersonate_url = $student->getImpersonateRedirect();

            }
            $students = $students->unique('id');

            $students = CollectionHelper::paginate($students, 25);
        }

        return response()->json($students);
    }

    /**
     * Sign-off student from Challenge
     *
     * @param $classId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function signOffStudentFromChallenge($classId, $id)
    {
        /** @var Person $student */
        $student = Person::query()->find($id);

        /** @var Organisation $class */
        $class = Organisation::query()->find($classId);

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        $status = $student->signoffFromChallenge($schoolChallenge);

        return response()->json([
            'signedOff' => $status
        ]);
    }

    /**
     * Create Ride for student
     *
     * @param $id
     * @param NovaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRideForStudent($id, NovaRequest $request)
    {
        /** @var Person $student */
        $student = Person::query()->find($id);

        /** @var Bike $bike */
        $primaryBike = $student->getPrimaryBike();

        /** @var Ride $latestRide */
        $latestRide = Ride::query()->where([
            ['person_id', '=', $student->id],
            ['bike_id', '=', $primaryBike->id],
            ['created_at', '>', $schoolChallenge->start],
            ['created_at', '<', $schoolChallenge->end]
        ])->latest()->first();

        $ride = new Ride();
        $ride->person_id = $student->id;
        $ride->bike_id = $primaryBike->id;
        if ($primaryBike->isTachobike()) {
            if (!is_null($latestRide)) {
                $ride->km_start = $latestRide->km_end;
            } else {
                $ride->km_start = 0;
            }
            $ride->km_end = $request->input('ride_value');
            $ride->km_cache = $ride->km_end - $ride->km_start;
        } else {
            $ride->km_cache = $request->input('ride_value');
        }
        $status = $ride->save();

        return response()->json([
            'createdRide' => $status
        ]);
    }

    /**
     * Sign-up student for Challenge
     *
     * @param $classId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function signUpStudentForChallenge($classId, $id)
    {
        /** @var Person $student */
        $student = Person::query()->find($id);

        /** @var Organisation $class */
        $class = Organisation::query()->find($classId);

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);
        // Try to signoff from challenge if previous registrations
        $student->signoffFromChallenge($schoolChallenge);

        // Create new registration
        Registration::query()->create([
            'person_id' => $student->id,
            'organisation_id' => $classId,
            'challenge_id' => $schoolChallenge->id
        ]);

        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Get available students - used for AJAX search
     *
     * @param $classId
     * @param NovaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableStudents($classId, NovaRequest $request)
    {
        $classes = $this->fwbService->getClassesForAdmin();

        $parent = null;
        // Try to get parent from administered classes
        /** @var Organisation $class */
        foreach ($classes as $class) {
            if ($class->hasParent()) {
                $parent = $class->parent;
                break;
            }
        }

        // Parent is either parent of a class
        if (!is_null($parent)) {
            $parentId = $parent->id;
        } else { // or class itself
            $parentId = $classes->first()->id;
        }

        // Take all child organisations (classes) of parent
        $classes = Organisation::query()->where('parent_id', '=', $parentId)->get();

        // Get school Challenge
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($classes->first());

        $students = new Collection();

        // Add all students from all classes into one collection
        /** @var Organisation $class */
        foreach ($classes as $class) {
            if ($class->id == $classId) { // Don't take class from which you are performing search
                continue;
            }
            // Get all students for this class with registration for the current or predecessor (if any) challenge
            $classStudents = $this->fwbService->getClassStudents($class->id, $schoolChallenge);
            $students = $students->merge($classStudents);
        }

        // Students created by the admin, without class
        $createdStudents = Person::query()
            ->whereHas('audits', function($query) {
                $query->where('person_id', Auth::user()->id)
                    ->where('event', 'created');
            })->get();

        // Merge all students and get unique ones
        $students = $students->merge($createdStudents);
        $students = $students->unique('id');

        /** @var Person $student */
        foreach ($students as $student) {
            $lastRegistration = $student->getLastRegistrationForChallenge($schoolChallenge);
            if (!is_null($lastRegistration)) {
                $student->registration_class = Organisation::query()->where('id', '=', $lastRegistration->organisation_id)->first('name');
            }
        }

        if ($request->filled('query')) {
            $searchQuery = $request->input('query');
            $result = $students->filter(function (Person $student) use ($searchQuery) {
                return strstr($student->firstname, $searchQuery) || strstr($student->lastname, $searchQuery) || strstr($student->id, $searchQuery);
            });
        } else {
            $result = null;
        }

        return response()->json($result);
    }

    /**
     * Delete student
     *
     * @param $studentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStudent($studentId)
    {
        $status = Person::query()
            ->where('id', '=', $studentId)
            ->delete();

        return response()->json([
            'success' => $status
        ]);
    }
}
