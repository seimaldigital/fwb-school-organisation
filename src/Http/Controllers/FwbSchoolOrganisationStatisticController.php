<?php

namespace External\FwbSchoolOrganisation\Http\Controllers;

use App\Challenge;
use App\Organisation;
use App\Services\StatisticsCalculator;
use External\FwbSchoolOrganisation\FwbSchoolOrganisationService;
use Illuminate\Support\Collection;

class FwbSchoolOrganisationStatisticController
{
    /**
     * @var FwbSchoolOrganisationService
     */
    public FwbSchoolOrganisationService $fwbService;

    /**
     * FwbSchoolOrganisationStatisticController constructor.
     *
     * @param FwbSchoolOrganisationService $fwbService
     */
    public function __construct(FwbSchoolOrganisationService $fwbService)
    {
        $this->fwbService = $fwbService;
    }

    /**
     * Get classes statistics for chart
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChartStatistics()
    {
        $classes = $this->fwbService->getClassesForAdmin();

        $parent = null;
        // Try to get parent from administered classes
        /** @var Organisation $class */
        foreach ($classes as $class) {
            if ($class->hasParent()) {
                $parent = $class->parent;
                break;
            }
        }

        // Define parent id for query in case parent exists
        if (!is_null($parent)) {
            $parentId = $parent->id;
        } else { // Otherwise parent is the first class in collection
            $parentId = $classes->first()->id;
        }

        // Take all child organisations (classes) of parent
        $classes = Organisation::query()->where('parent_id', '=', $parentId)->get();

        // Get school Challenge
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($classes->first());

        $classNames = new Collection();
        $statistics = new Collection();

        // Fill collections of class names and statistics
        /** @var Organisation $class */
        foreach ($classes as $class) {
            $classNames = $classNames->add($class->name);
            $statisticsCalculator = new StatisticsCalculator();
            $classStatistics = $statisticsCalculator->calculateStatisticsForOrganisation($class, $schoolChallenge);
            if ($classStatistics != false) {
                unset(
                    $classStatistics->km_average,
                    $classStatistics->height_meters_total,
                    $classStatistics->co2,
                    $classStatistics->kcal,
                    $classStatistics->kcal_average,
                    $classStatistics->money_saved,
                    $classStatistics->days_tracked
                );
            }
            $statistics = $statistics->add($classStatistics);
            unset($statisticsCalculator, $classStatistics);
        }

        return response()->json([
            'labels' => $classNames,
            'statistics' => $statistics
        ]);
    }
}

