<?php

namespace External\FwbSchoolOrganisation\Http\Controllers;

use App\Challenge;
use App\Organisation;
use App\Person;
use App\Registration;
use App\Stakeholder;
use Carbon\Carbon;
use External\FwbSchoolOrganisation\FwbSchoolOrganisationService;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use PhpOffice\PhpSpreadsheet\IOFactory;

class FwbSchoolOrganisationController
{
    public FwbSchoolOrganisationService $fwbService;

    /**
     * FwbSchoolOrganisationController constructor.
     *
     * @param FwbSchoolOrganisationService $fwbService
     */
    public function __construct(FwbSchoolOrganisationService $fwbService)
    {
        $this->fwbService = $fwbService;
    }

    /**
     * Get all classes for admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClasses()
    {
        $classes = $this->fwbService->getClassesForAdmin();

        if ($classes->isNotEmpty()) {
            $parent = null;
            // Try to get parent from administered classes
            /** @var Organisation $class */
            foreach ($classes as $class) {
                if ($class->hasParent()) {
                    $parent = $class->parent;
                    break;
                }
            }

            // Define parent id for query in case parent exists
            if (!is_null($parent)) {
                $parentId = $parent->id;
            } else { // Otherwise parent is the first class in collection
                $parentId = $classes->first()->id;
            }

            // Take all child organisations (classes) of parent
            $classes = Organisation::query()->where('parent_id', '=', $parentId)->latest()->get();

            // Get school Challenge
            $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($classes->first());

            // Attach registration status (singup status) and challenge to the response on every class
            /** @var Organisation $class */
            foreach ($classes as $class) {
                if (!is_null($schoolChallenge)) {
                    if ($class->isRegisteredToChallenge($schoolChallenge)) {
                        $registered = true;
                    } else {
                        $registered = false;
                    }
                } else {
                    $registered = false;
                }
                // Attach registered status and challenge
                $class->registered = $registered;
                $class->challenge = $schoolChallenge;

                // Check if any student is registered with this class for challenge
                $registeredStudent = Registration::query()
                    ->where([
                        ['organisation_id', '=', $class->id],
                        ['challenge_id', '=', $schoolChallenge->id]
                    ])
                    ->whereNotNull('person_id')
                    ->first();
                if (is_null($registeredStudent)) {
                    $class->no_students_registered = true;
                }

                // Attach year of last participated challenge
                $lastParticipatedChallengeId = optional(Registration::query()
                    ->where('organisation_id', '=', $class->id)
                    ->latest()
                    ->first())->challenge_id;

                if (!is_null($lastParticipatedChallengeId)) {
                    $lastParticipatedChallengeEndDate = Challenge::query()
                        ->find($lastParticipatedChallengeId)
                        ->end;
                    $class->last_participated_challenge_year = Carbon::parse($lastParticipatedChallengeEndDate)->year;
                }
                $class->created_year = Carbon::parse($class->created_at)->year;
            }
        }

        return response()->json($classes);
    }

    /**
     * Get all classes for dropdown
     *
     * This method is faster then getClasses as it doesn't perform
     * additional queries but returns only classes and parent
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClassesForDropdown()
    {
        $classes = $this->fwbService->getClassesForAdmin();

        $parent = null;
        // Try to get parent from administered classes
        /** @var Organisation $class */
        foreach ($classes as $class) {
            if ($class->hasParent()) {
                $parent = $class->parent;
                break;
            }
        }

        // Get correct parent id
        if (!is_null($parent)) {
            $parentId = $parent->id;
        } else {
            $parent = $classes->first();
            $parentId = $parent->id;
        }

        // Take all child organisations (classes) of parent
        $classes = Organisation::query()->where('parent_id', '=', $parent->id)->get();
        // Add parent to collection to display it
        // in dropdown on student creation
        $classes = $classes->toBase()->add($parent);

        return response()->json($classes);
    }

    /**
     * Get single class
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClass($id)
    {
        /** @var Organisation $class */
        $class = Organisation::query()->find($id);

        // Attach year of last participated challenge
        $lastParticipatedChallengeId = optional(Registration::query()
            ->where('organisation_id', '=', $id)
            ->latest()
            ->first())->challenge_id;

        if (!is_null($lastParticipatedChallengeId)) {
            $lastParticipatedChallengeEndDate = Challenge::query()
                ->find($lastParticipatedChallengeId)
                ->end;
            $class->last_participated_challenge_year = Carbon::parse($lastParticipatedChallengeEndDate)->year;
        }
        // Attach year when class was created
        $class->created_year = Carbon::parse($class->created_at)->year;

        return response()->json($class);
    }

    /**
     * Get School where user is admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSchool()
    {
        $classes = $this->fwbService->getClassesForAdmin();

        $parent = null;
        /** @var Organisation $class */
        foreach ($classes as $class) {
            $parent = $class->parent;
            break;
        }

        if (is_null($parent)) {
            $parent = $classes->first();
        }

        return response()->json($parent);
    }

    /**
     * Create single class
     *
     * @param NovaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createClass(NovaRequest $request)
    {
        // Validate inputs, return errors on failed validation
        $validation = $this->validateClass($request->all());
        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        $people = new Collection();
        // try to process excel if there is one before creating a class
        if (!is_null($request->file('people_list'))) {

            $people = $this->fwbService->createPeopleFromExcel($request->file('people_list'));

            // return excel validation error if there is some
            if (!($people instanceof Collection) && (is_array($people) && array_key_exists('danger', $people))) {
                return response()->json([
                    'errors' => ['people_list' => [$people['danger']]]
                ]);
            }
        }

        // Get class for admin
        /** @var Organisation $administratedClass */
        $administratedClass = $this->fwbService->getClassesForAdmin()->first();

        // If there are children, this is a school - top level,
        // and we take that top level id
        if ($administratedClass->hasChildren()) {
            $parentId = $administratedClass->id;
        } else { // otherwise take from parent_id column
            $parentId = $administratedClass->parent_id;
        }

        // Take stakeholder id from administrated class
        $stakeholderId = $administratedClass->stakeholder_id;

        // Create class
        /** @var Organisation $class */
        $class = Organisation::query()->create([
            'name' => $request->input('class_name'),
            'number_of_people' => $request->input('number_of_people'),
            'parent_id' => $parentId,
            'type' => 1,
            'approval_status' => 1,
            'stakeholder_id' => $stakeholderId
        ]);

        // Attach class to the admin
        $this->fwbService->attachClassToAdmin($class);

        // Get school Challenge for Stakeholder
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        // Signup class to the Challenge in case of one
        if (!is_null($schoolChallenge)) {
            $class->signupToChallenge($schoolChallenge);
        }

        // process people created from excel
        if ($people instanceof Collection) {
            // Create people registrations to the organisation and challenge (aka class)
            foreach ($people as $person) {
                $registration = new Registration();
                $registration->person_id = $person->id;
                $registration->organisation_id = $class->id;
                if (!is_null($schoolChallenge)) {
                    $registration->challenge_id = $schoolChallenge->id;
                }
                $registration->save();
                unset($registration);
            }
        }

        // Return response
        return response()->json($class);
    }
    /**
     * Validate class
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateClass(array $data)
    {
        $rules = [
            'class_name' => ['required','string','max:20'],
            'number_of_people' => ['required', 'digits_between:1,3'],
            'people_list' => ['nullable', 'mimes:csv,xlsx,xls,ods'],
        ];

        $messages = [
            'class_name.required' => trans('Onboarding.Pflichtfeld'),
            'number_of_people.required' => trans('Onboarding.Pflichtfeld'),
            'number_of_people.digits_between' => 'Number must be between 1 and 3 digits',
            'people_list.mimes' => 'Non suported type, please add .csv, .xlsx, .xls or .ods file'
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Update single class
     *
     * @param $id
     * @param NovaRequest $request
     */
    public function updateClass($id, NovaRequest $request)
    {
        // Validate inputs, return errors on failed validation
        $validation = $this->validateClass($request->all());
        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        /** @var Organisation $organisation */
        $organisation = Organisation::query()->find($id);

        $status = $organisation->update([
            'name' => $request->input('class_name'),
            'number_of_people' => $request->input('number_of_people')
        ]);

        return response()->json([
            'succes' => $status
        ]);
    }

    /**
     * Delete single class
     *
     * @param $id
     */
    public function deleteClass($id)
    {
        $status = Organisation::query()
            ->where('id', '=', $id)
            ->delete();

        return response()->json([
            'success' => $status
        ]);
    }

    /**
     * Get class singup status for the Challenge
     *
     * @param $id
     */
    public function getClassSingupStatus($id)
    {
        /** @var Organisation $class */
        $class = Organisation::query()->find($id);

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $this->fwbService->getSchoolChallengeForStakeholder($class);

        if (!is_null($schoolChallenge)) {
            // Check if class is registered and set status of class
            if ($class->isRegisteredToChallenge($schoolChallenge)) {
                $registered = true;
            } else {
                $registered = false;
            }
            // Check if any student is registered with this class for challenge
            $registeredStudent = Registration::query()
                ->where([
                    ['organisation_id', '=', $id],
                    ['challenge_id', '=', $schoolChallenge->id]
                ])
                ->whereNotNull('person_id')
                ->first();
        } else {
            $registered = false;
        }

        // Set status of registered students
        if (is_null($registeredStudent)) {
            $studentsRegistered = false;
        } else {
            $studentsRegistered = true;
        }

        return response()->json([
            'studentsRegistered' => $studentsRegistered,
            'registered' => $registered,
            'challenge' => $schoolChallenge
        ]);
    }

    /**
     * Validate class signup (modal)
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateClassSignup(array $data)
    {
        $rules = [
            'name' => ['required','string','max:20'],
            'numberOfPeople' => ['nullable', 'digits_between:1,3'],
            'withStudents' => ['nullable'],
        ];
        $messages = [
            'name.required' => trans('Onboarding.Pflichtfeld'),
            'numberOfPeople.digits_between' => 'Number must be between 1 and 3 digits',
        ];

        return Validator::make($data, $rules, $messages);
    }

    /**
     * Signup class to the challenge
     *
     * @param NovaRequest $request
     * @param $id
     * @param $challengeId
     * @return \Illuminate\Http\JsonResponse
     *
     * @todo Add some validation for the request inputs
     */
    public function signupClassToChallenge(NovaRequest $request, $id, $challengeId)
    {
        // Validate inputs, return errors on failed validation
        $validation = $this->validateClassSignup($request->all());
        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        /** @var Organisation $class */
        $class = Organisation::query()->find($id);
        /** @var Challenge $challenge */
        $challenge = Challenge::query()->find($challengeId);

        $class->update([
            'name' => $request->input('name'),
            'number_of_people' => $request->input('numberOfPeople'),
        ]);

        $signupStatus = $class->signupToChallenge($challenge);

        if ($request->input('withStudents')) {
            // Get all students for this class with registration for the current or predecessor (if any) challenge
            $formerChallengeParticipants = $this->fwbService->getClassStudents($id, $challenge);

            // Reject ones with the registration within different class
            $formerChallengeParticipants->reject(function (Person $student) use ($id, $challenge) {
                return $student->registrations()
                    ->where('organisation_id', '!=', $id)
                    ->where('challenge_id', '=', $challenge->id)->exists();
            });


            foreach ($formerChallengeParticipants as $formerChallengeParticipant) {
                Registration::query()->create([
                    'organisation_id' => $id,
                    'challenge_id' => $challengeId,
                    'person_id' => $formerChallengeParticipant->id,
                ]);
            }
        }

        return response()->json([
            'signupStatus' => $signupStatus
        ]);
    }

    /**
     * Get logged in User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLoggedInUser()
    {
        /** @var Person $user */
        $user = Auth::user();
        
        return response()->json($user);
    }

    /**
     * Download excel template
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadExcelTemplate()
    {
        $fileLocation = public_path('templates/');
        $fileName = 'Teilnehmer_anmelden.xlsx';
        $filePath = sprintf('%s%s', $fileLocation, $fileName);
        $header = [
            'Content-Type' => 'application/*',
        ];
        return response()->download($filePath, $fileName, $header);
    }
}
