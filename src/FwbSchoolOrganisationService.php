<?php


namespace External\FwbSchoolOrganisation;


use App\Challenge;
use App\Organisation;
use App\Person;
use App\Stakeholder;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Actions\Action;
use PhpOffice\PhpSpreadsheet\IOFactory;

class FwbSchoolOrganisationService
{
    /**
     * Get school Challenge for Stakeholder
     *
     * @param Organisation $class
     * @return Challenge
     */
    public function getSchoolChallengeForStakeholder(Organisation $class)
    {
        /** @var Stakeholder $stakeholder */
        $stakeholder = $class->stakeholder;

        // Take all stakeholder challenges with open signup for organisations
        $stakeholderChallenges = $stakeholder->openOrganisationSignupChallenges();
        // Filter them by type of school
        $stakeholderSchoolChallenges = $stakeholderChallenges->filter(function (Challenge $challenge) use ($class) {
            if ($challenge->getOrganisationTypeLimits()->contains($class->organisationType())) {
                return true;
            }
            return false;
        });

        /** @var Challenge $schoolChallenge */
        $schoolChallenge = $stakeholderSchoolChallenges->sortByDesc('end')->first();

        return $schoolChallenge;
    }

    /**
     * Get all students for this class with registration for the current or predecessor (if any) challenge
     *
     * @param $classId
     * @param Challenge $schoolChallenge
     * @param null $withCurrentChallengeRegistration
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getClassStudents($classId, Challenge $schoolChallenge, $withCurrentChallengeRegistration = null)
    {
        if (!is_null($withCurrentChallengeRegistration) || !$schoolChallenge->hasPredecessor()) {
            $challengeIds = [$schoolChallenge->id];
        } else {
            $challengeIds = [$schoolChallenge->id, $schoolChallenge->predecessor->id];
        }

        $students = Person::query()
            ->whereIn('id', function (Builder $query) use ($classId, $challengeIds) {
                $query->select('person_id')
                    ->from('registrations')
                    ->where('organisation_id', '=', $classId)
                    ->whereNull('deleted_at')
                    ->whereIn('challenge_id', $challengeIds);
            })->get();

        return $students;
    }

    /**
     * Get all classes for admin
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getClassesForAdmin()
    {
        /** @var Person $admin */
        $admin = Auth::user();

        return $admin->organisations()
            ->has('school')
            ->get();
    }

    /**
     * Attach class to the admin
     *
     * @param Organisation $class
     */
    public function attachClassToAdmin(Organisation $class)
    {
        /** @var Person $admin */
        $admin = Auth::user();
        $admin->selectOrganisation($class);
    }

    /**
     * Create people from Excel file
     *
     * @param $file
     * @return array|Collection|string[]
     */
    public function createPeopleFromExcel($file)
    {
        // Load file
        $spreadsheet = IOFactory::load($file);
        // Get active sheet
        $worksheet = $spreadsheet->getActiveSheet();
        // Convert it to array
        $rows = $worksheet->toArray();

        $people = new Collection();
        $alphabet = range('A', 'Z');

        // key is the row count(starts from 0)
        foreach ($rows as $key => $row) {
            if ($key == 0) {
                continue; // skip first row -> title
            }

            $person = new Person();

            for ($x = 0; $x <= 9; $x++) {
                if (in_array($x, [2,3,4,5,6,9])) { // those are the columns that can be null
                    continue;
                }
                if ($row[$x] == "" || $row[$x] == null) {
                    return Action::danger(
                        sprintf('%s%s%s%s%s', 'Fehler in Zeile ', $key, ' Spalte ', $alphabet[$x], ' - Wert erforderlich!')
                    );
                }
            }

            $person->firstname = $row[0];
            $person->lastname = $row[1];
            $person->street = $row[2];
            $person->housenr = $row[3];
            $person->postcode = $row[4];
            $person->city = $row[5];
            $person->email = $row[6];

            if ($row[7] == "" || $row[7] == null) {
                $person->username = $row[6];
            } else {
                $person->username = $row[7];
            }

            $person->password = $row[8];

            if (strtolower($row[9]) == 'ja') {
                $person->newsletter_off = false;
            } else {
                $person->newsletter_off = true;
            }

            $people->add($person);
        }

        foreach ($people as $person) {
            $person->save();
        }

        // return collection of people
        return $people;
    }
}
